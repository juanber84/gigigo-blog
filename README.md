Quick-start guide
=================

For the impatient, here’s how to get a boilerplate Jekyll site up and running.

	~ $ gem install jekyll
	~ $ jekyll new myblog
	~ $ cd myblog
	~/myblog $ jekyll serve
	# => Now browse to http://localhost:4000

